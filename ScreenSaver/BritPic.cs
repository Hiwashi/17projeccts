﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ScreenSaver
{
    class BritPic
    {
        public int PicNum { get; set; }
        public float X { get; set; }
        public float Y { get; set; }
        public float Speed { get; set; }
    }
}
